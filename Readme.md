Mailbox app
============

# Requirements

- PHP 7.1
- Apache or nginx
- MySQL

# Getting Started

1. Clone this repo
2. Go to the cloned directory and run `composer install`
3. Make sure to properly setup write permissions for `storage` and `vendor`folders.
4. Copy `env.testing.example` to `.env.testing`
5. Create two databases, one for prod and one for testing. Make sure to update `.env` and `.env.testing` accordingly.
6. Run migrations and database seed.
7. Run tests `make test`

# API Documentation

### Authentication

All endpoints listed below requires ['Basic' HTTP Authentication](https://tools.ietf.org/html/rfc7617).

As a demo credentials, you can use: `mailbox.it@email.com / mailbox`

### List messages

**GET** `api/v1/messages?limit=1&page=1`

```json
{
   "data":[
      {
         "uid":"21",
         "sender":"Ernest Hemingway",
         "subject":"animals",
         "message":"This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
         "time_sent":1459239867,
         "is_read":false
      }
   ],
   "paginator":{
      "total":6,
      "next":"http://app.mailbox.dev/api/v1/messages?limit=1&page=2",
      "hasMorePages":true,
      "prev":null,
      "perPage":"1"
   }
}
```

### List archived messages

**GET** `api/v1/messages?archived=1&limit=1&page=1`

```json
{
   "data":[
      {
         "uid":"21",
         "sender":"Ernest Hemingway",
         "subject":"animals",
         "message":"This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and end with a holiday celebration.",
         "time_sent":1459239867,
         "is_read":true
      }
   ],
   "paginator":{
      "total":6,
      "next":"http://app.mailbox.dev/api/v1/messages?limit=1&page=2",
      "hasMorePages":true,
      "prev":null,
      "perPage":"1"
   }
}
```

### Show message

**GET** `api/v1/messages/21`

```json
{
   "data":{
      "uid":21,
      "sender":"Ernest Hemingway",
      "subject":"animals",
      "message":"This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
      "time_sent":1459239867,
      "is_read":false,
      "is_archived":false
   }
}
```

**GET** `api/v1/messages/notfoundmessage`

```json
{
  "error": "Element not found!"
}
```

### Mark message as read

**PATCH** `api/v1/messages/21/read`

```json
{
   "message": "Message successfully marked as read!",
   "data":{
      "uid":21,
      "sender":"Ernest Hemingway",
      "subject":"animals",
      "message":"This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
      "time_sent":1459239867,
      "is_read":true,
      "is_archived":false
   }
}
```

**GET** `api/v1/messages/notfoundmessage/read`

```json
{
  "error": "Element not found!"
}
```

### Archive a message

**PATCH** `api/v1/messages/21/archive`

```json
{
   "message": "Message successfully archived!",
   "data":{}
}
```

**GET** `api/v1/messages/notfoundmessage/archive`

```json
{
  "error": "Element not found!"
}
```
