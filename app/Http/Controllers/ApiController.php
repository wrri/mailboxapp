<?php


namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;


class ApiController extends Controller
{
    private const LIMIT = 20;

    protected function respondWithPaginator(LengthAwarePaginator $paginator, array $data): array
    {
        return array_merge($this->respond($data), ['paginator' => [
            'total' => $paginator->total(),
            'next' => $paginator->nextPageUrl(),
            'hasMorePages' => $paginator->hasMorePages(),
            'prev' => $paginator->previousPageUrl(),
            'perPage' => $paginator->perPage(),
        ]]);
    }

    protected function respond($data, $message = null): array
    {
        $response = [
            'data' => $data,
        ];

        if ($message) {
            $response['message'] = $message;
        }

        return $response;
    }

    protected function respondWithNotFound(string $message = "Element not found!")
    {
        return $this->respondWithError($message, Response::HTTP_NOT_FOUND);
    }

    protected function respondWithInternalError(string $message = "Internal server error!")
    {
        return $this->respondWithError($message, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    protected function getLimit(): string
    {
        $limit = Input::get('limit') ?: self::LIMIT;
        if ($limit > self::LIMIT) {
            $limit = self::LIMIT;
        }

        return $limit;
    }

    private function respondWithError(string $message, string $statusCode)
    {
        return response(['error' => $message], $statusCode)
            ->header('Content-Type', "application/json");
    }
}
