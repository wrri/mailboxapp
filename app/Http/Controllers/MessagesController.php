<?php

namespace App\Http\Controllers;

use App\MailBox\Repository\MessageRepository;
use App\MailBox\Transformers\MessagesTransformer;
use App\Message;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Psr\Log\LoggerInterface;

class MessagesController extends ApiController
{

    /**
     * @var MessagesTransformer
     */
    private $messagesTransformer;

    /**
     * @var MessageRepository
     */
    private $messageRepo;

    public function __construct(MessagesTransformer $messagesTransformer, MessageRepository $messageRepo)
    {
        $this->messagesTransformer = $messagesTransformer;
        $this->messageRepo = $messageRepo;
    }


    public function index()
    {
        $messages = $this->messageRepo->findMessageList(
            Input::get('archived'),
            $this->getLimit()
        );

        return $this->respondWithPaginator(
            $messages->appends(request()->input()),
            $this->messagesTransformer->transformCollection($messages->items())
        );
    }

    public function show($message)
    {
        try {
            return $this->respond(Message::findOrFail($message));

        } catch (ModelNotFoundException $exception) {
            return $this->respondWithNotFound();
        }
    }

    public function read($messageUid)
    {
        try {
            $message = Message::findOrFail($messageUid);
            $message->is_read = true;
            $message->save();

            return $this->respond(
                $this->messagesTransformer->transform($message),
                'Message successfully marked as read!'
            );

        } catch (ModelNotFoundException $exception) {
            return $this->respondWithNotFound();
        } catch (\Exception $exception) {
            Log::critical(sprintf('Unable to mark message [uid: %s] as read. Error: %s', $messageUid, $exception->getMessage()));
            return $this->respondWithInternalError();
        }
    }

    public function archive($messageUid)
    {
        try {
            $message = Message::findOrFail($messageUid);
            $message->is_archived = true;
            $message->save();

            return $this->respond([], 'Message successfully archived!');
        } catch (ModelNotFoundException $exception) {
            return $this->respondWithNotFound();
        } catch (\Exception $exception) {
            Log::critical(sprintf('Failed to archive message [uid: %s]. Error: %s', $messageUid, $exception->getMessage()));
            return $this->respondWithInternalError();
        }
    }
}
