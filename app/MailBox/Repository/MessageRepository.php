<?php

namespace App\MailBox\Repository;

use App\Message;
use Illuminate\Pagination\LengthAwarePaginator;

class MessageRepository
{
    public function findMessageList($archived = null, $limit = 10): LengthAwarePaginator
    {
        if ($archived) {
            return $messages =  Message::where('is_archived', '=', $archived)
                ->paginate($limit);
        }

        return Message::paginate($limit);
    }

}
