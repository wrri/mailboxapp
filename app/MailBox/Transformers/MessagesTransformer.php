<?php

namespace App\MailBox\Transformers;

class MessagesTransformer extends ModelTransformer
{

    public function transform($message)
    {
        return [
            'uid' =>  (string) $message->uid,
            'sender' =>  $message->sender,
            'subject' =>  $message->subject,
            'message' =>  $message->message,
            'time_sent' => $message->time_sent,
            'is_read' =>  $message->is_read,
        ];
    }
}
