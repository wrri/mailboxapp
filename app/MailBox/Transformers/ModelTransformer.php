<?php

namespace App\MailBox\Transformers;

use Illuminate\Database\Eloquent\Model;

abstract class ModelTransformer
{

    public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }

    abstract public function transform($item);
}
