<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $primaryKey = 'uid';
    public $timestamps = false;

    public function getIsReadAttribute($value)
    {
        return (boolean) $value;
    }

    public function getIsArchivedAttribute($value)
    {
        return (boolean) $value;
    }

    public function getTimeSentAttribute($value)
    {
        return (int) (new \DateTime($value))->format('U');
    }
}
