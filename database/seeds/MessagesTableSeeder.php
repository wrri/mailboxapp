<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('messages')->truncate();

        $messages = file_get_contents(__DIR__.'/messages_sample.json');
        $messages = json_decode($messages, true);
        array_map(function($message) {
            $message['time_sent'] = (new \DateTime())->setTimestamp($message['time_sent']);
            DB::table('messages')->insert($message);
        }, $messages['messages']);
    }
}
