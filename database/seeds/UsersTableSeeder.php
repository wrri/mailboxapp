<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        DB::table('users')->insert([
            'name' => 'mailbox',
            'email' =>'mailbox.it@email.com',
            'password' => bcrypt('mailbox'),
        ]);
    }
}
