<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/messages', 'MessagesController@index');
Route::get('/messages/{message}', 'MessagesController@show');
Route::patch('/messages/{message}/read', 'MessagesController@read');
Route::patch('/messages/{message}/archive', 'MessagesController@archive');
