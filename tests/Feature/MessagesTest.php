<?php

namespace Tests\Feature;

use Tests\MailBoxTestCase;

class MessagesTest extends MailBoxTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->seed(\UsersTableSeeder::class);
    }

    private function authenticate(string $uri, string $method = 'GET')
    {
        $response = $this->call($method, $uri, [], [], [], ['PHP_AUTH_USER' => 'mailbox.it@email.com', 'PHP_AUTH_PW' => 'mailbox']);
        return $response;
    }

    public function testAuthentication()
    {
        $response = $this->getJson('api/v1/messages');
        $response->assertStatus(401);
        $response = $this->authenticate('api/v1/messages');
        $response->assertSuccessful();
    }

     public function testSuccessfullyFetchesMessages()
     {
        $response = $this->authenticate('api/v1/messages');
        $response->assertSuccessful();
        $response->assertJsonCount(6, 'data');

        $responseJson = $response->decodeResponseJson();
        $this->assertArrayHasKey('data', $responseJson);
        $responseJsonData = $responseJson['data'];
        $this->assertEquals($this->getFirstMessage(), $responseJsonData[0]);
    }

    public function testSuccessfullyFetchesAndPaginateMessages()
    {
        $response = $this->authenticate('api/v1/messages');
        $response->assertSuccessful();
        $response->assertJsonCount(6, 'data');

        $responseJson = $response->decodeResponseJson();
        $this->assertArrayHasKey('paginator', $responseJson);
        $paginator = $responseJson['paginator'];
        $this->assertEquals([
            'total' => 6,
            'next' => null,
            'hasMorePages' => false,
            'prev' => null,
            'perPage' => 20
        ], $paginator);

        $response = $this->authenticate('api/v1/messages?limit=2&page=1');
        $response->assertSuccessful();
        $response->assertJsonCount(2, 'data');

        $responseJson = $response->decodeResponseJson();
        $this->assertArrayHasKey('paginator', $responseJson);
        $paginator = $responseJson['paginator'];
        $this->assertEquals(6, $paginator['total']);
        $this->assertTrue($paginator['hasMorePages']);
        $this->assertNull($paginator['prev']);
        $this->assertEquals(2, $paginator['perPage']);
        $this->assertContains('api/v1/messages?limit=2&page=2', $paginator['next']);
    }

    public function testSuccessfullyFetchesArchivedMessages()
    {
        $response = $this->authenticate('api/v1/messages?archived=1');
        $response->assertSuccessful();
        $response->assertJsonCount(0, 'data');

        $this->archiveMessage(21);
        $response = $this->getJson('api/v1/messages?archived=1');
        $response->assertSuccessful();
        $response->assertJsonCount(1, 'data');
        $this->assertEquals($this->getFirstMessage(), $response->decodeResponseJson()['data'][0]);

    }

    public function testSuccessfullyFetchesPaginatedArchivedMessages()
    {
        $this->archiveMessage(21);
        $this->archiveMessage(22);

        $response = $this->authenticate('api/v1/messages?archived=1&limit=1');
        $response->assertSuccessful();
        $response->assertJsonCount(1, 'data');
        $this->assertEquals($this->getFirstMessage(), $response->decodeResponseJson()['data'][0]);

        $responseJson = $response->decodeResponseJson();
        $this->assertArrayHasKey('paginator', $responseJson);
        $paginator = $responseJson['paginator'];
        $this->assertEquals(2, $paginator['total']);
        $this->assertTrue($paginator['hasMorePages']);
        $this->assertNull($paginator['prev']);
        $this->assertEquals(1, $paginator['perPage']);
        $this->assertContains('api/v1/messages?archived=1&limit=1&page=2', $paginator['next']);
    }

    public function testSuccessfullyShowSingleMessage()
    {
        $response = $this->authenticate('api/v1/messages/21');
        $response->assertSuccessful();

        $responseJson = $response->decodeResponseJson();
        $this->assertArrayHasKey('data', $responseJson);
        $responseJsonData = $responseJson['data'];
        $message = $this->getFirstMessage();
        $message['is_archived'] = false;
        $this->assertEquals($message, $responseJsonData);
    }

    public function testTryingToShowNotFoundMessage()
    {
        $response = $this->authenticate('api/v1/messages/foo');
        $response->assertStatus(404);

        $responseJson = $response->decodeResponseJson();
        $this->assertArrayHasKey('error', $responseJson);
        $this->assertEquals('Element not found!', $responseJson['error']);
    }

    public function testSuccessfullyReadAndShowSingleMessage()
    {
        $message = $this->getMessage(21);
        $this->assertFalse($message->is_read);

        $response = $this->authenticate( 'api/v1/messages/21/read', 'PATCH');
        $response->assertSuccessful();

        $responseJson = $response->decodeResponseJson();
        $this->assertArrayHasKey('data', $responseJson);
        $this->assertArrayHasKey('message', $responseJson);
        $message = $this->getFirstMessage();
        $message['is_read'] = true;
        $this->assertEquals($message, $responseJson['data']);
        $this->assertEquals('Message successfully marked as read!', $responseJson['message']);
    }

    public function testTryingToReadNotFoundMessage()
    {
        $response = $this->authenticate( 'api/v1/messages/foo/read', 'PATCH');
        $response->assertStatus(404);

        $responseJson = $response->decodeResponseJson();
        $this->assertArrayHasKey('error', $responseJson);
        $this->assertEquals('Element not found!', $responseJson['error']);
    }

    public function testSuccessfullyArchivingSingleMessage()
    {
        $message = $this->getMessage(21);
        $this->assertFalse($message->is_archived);

        $response = $this->authenticate( 'api/v1/messages/21/archive','PATCH');
        $response->assertSuccessful();

        $responseJson = $response->decodeResponseJson();
        $this->assertArrayHasKey('message', $responseJson);
        $message = $this->getMessage(21);
        $this->assertTrue($message->is_archived);
        $this->assertEquals('Message successfully archived!', $responseJson['message']);
    }

    public function testTryingToArchiveNotFoundMessage()
    {
        $response = $this->authenticate( 'api/v1/messages/foo/archive', 'PATCH');
        $response->assertStatus(404);

        $responseJson = $response->decodeResponseJson();
        $this->assertArrayHasKey('error', $responseJson);
        $this->assertEquals('Element not found!', $responseJson['error']);
    }
}
