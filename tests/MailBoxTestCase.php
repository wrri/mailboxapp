<?php

namespace Tests;

use App\Message;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class MailBoxTestCase  extends TestCase
{

    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();
        $this->seed(\MessagesTableSeeder::class);
    }

    protected function getFirstMessage(): array
    {
        return [
            "uid" => "21",
            "sender" => "Ernest Hemingway",
            "subject" => "animals",
            "message" => "This is a tale about nihilism. The story is about a combative nuclear engineer who hates animals. It starts in a ghost town on a world of forbidden magic. The story begins with a legal dispute and ends with a holiday celebration.",
            "time_sent" => 1459239867,
            "is_read" => false,
        ];
    }

    protected function getMessage(string $message)
    {
        return Message::findOrFail($message);
    }

    protected function archiveMessage(string $message)
    {
        $message = $this->getMessage($message);
        if ($message) {
            $message->is_archived = true;
            $message->save();
        }
    }
}
