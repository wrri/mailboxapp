<?php

namespace Tests\Unit;

use App\MailBox\Repository\MessageRepository;
use Tests\MailBoxTestCase;

class MessagesRepositoryTest extends MailBoxTestCase
{
    /**
     * @var MessageRepository
     */
    private $messageRepo;

    public function setUp()
    {
        parent::setUp();
        $this->messageRepo = new MessageRepository();
    }

    public function testFindMessagesCollection()
    {
        $messages = $this->messageRepo->findMessageList();
        $this->assertCount(6, $messages);
        $firstMessageSeed = $this->getFirstMessage();
        $firstMessage = $messages[0];
        $this->assertEquals($firstMessageSeed['uid'], $firstMessage->uid);
        $this->assertEquals($firstMessageSeed['sender'], $firstMessage->sender);
        $this->assertEquals($firstMessageSeed['subject'], $firstMessage->subject);
        $this->assertEquals($firstMessageSeed['message'], $firstMessage->message);
    }

    public function testFindMessagesByLimit()
    {
        $messages = $this->messageRepo->findMessageList(null, 2);
        $this->assertCount(2, $messages);
    }

    public function testFindArchivedMessages()
    {
        $messages = $this->messageRepo->findMessageList(true);
        $this->assertCount(0, $messages);
        $this->archiveMessage(21);
        $messages = $this->messageRepo->findMessageList(true);
        $this->assertCount(1, $messages);
        $this->assertTrue((boolean) $messages[0]->is_archived);
    }

    public function testFindArchivedMessagesByLimit()
    {
        $this->archiveMessage(21);
        $this->archiveMessage(23);

        $messages = $this->messageRepo->findMessageList(true, 2);
        $this->assertCount(2, $messages);
        $this->assertTrue((boolean) $messages[0]->is_archived);
        $this->assertTrue((boolean) $messages[1]->is_archived);
    }
}
