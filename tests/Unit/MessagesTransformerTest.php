<?php

namespace Tests\Unit;

use App\MailBox\Transformers\MessagesTransformer;
use App\Message;
use Tests\MailBoxTestCase;

class MessagesTransformerTest extends MailBoxTestCase
{

    /**
     * @var MessagesTransformer
     */
    private $messagesTransformer;

    public function setUp()
    {
        parent::setUp();
        $this->messagesTransformer = new MessagesTransformer();
    }

    public function testTransformMessageModel()
    {
        $message = Message::findOrFail(21);
        $transformedMessage = $this->messagesTransformer->transform($message);
        $this->assertEquals($this->getFirstMessage(), $transformedMessage);
    }

    public function testTransformMessageCollection()
    {
        $message = Message::findOrFail(21);
        $transformedMessages = $this->messagesTransformer->transformCollection([$message]);
        $this->assertCount(1, $transformedMessages);
        $this->assertEquals($this->getFirstMessage(), $transformedMessages[0]);
    }
}
